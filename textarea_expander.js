Drupal.behaviors.textareaExpander = function() {
  $('textarea.resizable').each(function() {
    $('textarea#' + this.id).bind("keypress", {currId: this.id}, teEventHandler);
    $('textarea#' + this.id).css("height", 150); // We default to 150px.
    // Initial refresh
    teRefresh(this.id);
  });
  $('input.teaser-button').bind("click", teRefreshAllHandler); // Fire on a click on the teaser split/join button 
};

function teEventHandler(event) {
  teRefresh(event.data.currId);
}

function teRefreshAllHandler() { // Split/join teaser has been clicked, refresh all preview panes
  $('textarea.resizable').each(function() {
    teRefresh(this.id);
  });
}

function teRefresh(currId) {
  teResizeTextarea($('textarea#' + currId));
}

function teResizeTextarea(textarea) {
  var defaultHeight = 150;  // We default to 150px.
  var lines = textarea.val().split("\n");
  var count = lines.length;
  $.each(lines, function() { 
    count += parseInt(this.length / 70); 
  });
  var currHeight = parseInt(textarea.css("height"));
  var rows = parseInt(currHeight / 20) - 1;
  
  if (count > rows) {
    textarea.css("height", (currHeight + defaultHeight));
  }
  else if ((count + 8) <= rows && currHeight != defaultHeight) {
    textarea.css("height", (currHeight - defaultHeight));
  }
  else if (count <= rows) {
    textarea.css("height", currHeight);
  }
}

